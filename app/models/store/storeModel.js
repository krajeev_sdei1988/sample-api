var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var storeSchema = new Schema({
    name: {
        type: String,
        required: 'Please enter the store name.',
        match: [/^[A-Za-z0-9-'& ]*$/, "Only following characters (a-z A-Z 0-9 & - ') are allowed in company name."],
        min: [2, "Store Name is too short."],
        max: [100, "Store Name is too long."],
    },
    website: {
        type: String,
        match: [/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/, "Invalid website format."]
    },
    landmark: {
        type: String,
        match: [/^[A-Za-z0-9':,-_ ]*$/, "Only following characters (A-Za-z0-9':,-_) are allowed in Apt./Unit/Suite."]
    },
    streetNumber: {
        type: String,
        // required: 'Please enter the street.',
        match: [/^[A-Za-z0-9':,-_ ]*$/, "Only following characters (A-Za-z0-9':,-_) are allowed in Apt./Unit/Suite."]
    },
    address: {
        type: String,
        // required: 'Please enter the city,state and country.'
    },
    zipCode: {
        type: String,
        // required: "Please enter the zip code."
    },
    phone: {
        type: String,
        // required: "Please enter the phone number."
    },
    companyId: {
        type: Schema.Types.ObjectId,
        unique: true,
        required: 'CompanyId is required',
        ref: 'companies'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        // required: 'createdBy id is required.'
    },
    creatorType: {
        type: Number,
        enum: [1], //1-for superadmin
        // required: 'Document creator type is required.'
    },
    updatedOn: {
        type: Date
    },
    updatedBy: {
        type: Schema.Types.ObjectId
    },
    updaterType: {
        type: Number,
        enum: [1, 2] //1-superAdmin 2- admin
    }
}, {
    collection: 'stores'
});


storeSchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

storeSchema.statics.deserializeUser = function(obj, done) {
    done(null, obj);
};

var storeObj = mongoose.model('stores', storeSchema);
module.exports = storeObj;