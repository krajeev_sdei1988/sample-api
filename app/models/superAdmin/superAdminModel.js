var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var superAdminSchema = new Schema({
    firstName: {
        type: String,
        required: 'First name is required',
    },
    lastName: {
        type: String,
        required: 'Last name is required'
    },
    email: {
        type: String,
        required: 'Please enter valid email id.',
        // match: [constantObj.emailFormat, 'Please enter a valid email address.'],
        lowercase: true
    },
    userType: {
        type: Number,
        enum: [1],
        required: 'Please enter user type.' //1 -superadmin 2-admin 3-subAdmin 4-customer
    },
	loginId: {
		type: Schema.Types.ObjectId,
		required: 'Please enter login id.',
		unique: true,
		ref: 'login'
	},
    isDeleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date
    },
    profileImage: {
        type: String
    }
}, {
    collection: 'superAdmins'
});


superAdminSchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

superAdminSchema.statics.deserializeUser = function(obj, done) {
    done(null, obj);
};

var superAdminObj = mongoose.model('superAdmins', superAdminSchema);
module.exports = superAdminObj;