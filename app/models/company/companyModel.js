var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var companySchema = new Schema({
    name: {
        type: String,
        required: 'Please enter the company name.',
        match: [/^[A-Za-z0-9-'& ]*$/, "Only following characters (a-z A-Z 0-9 & - ') are allowed in company name."],
        min: [2, "Company Name is too short."],
        max: [100, "Company Name is too long."],
        unique: true
    },
    website: {
        type: String,
        match: [/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/, "Invalid website format."]
    },
    routeUrl: {
        type: String,
        required: 'Please enter the routeUrl for custome website.',  
        unique: true
    },
    landmark: {
        type: String,
        match: [/^[A-Za-z0-9':,-_ ]*$/, "Only following characters (A-Za-z0-9':,-_) are allowed in Apt./Unit/Suite."]
    },
    streetNumber: {
        type: String,
        // required: 'Please enter the street.',
        match: [/^[A-Za-z0-9':,-_ ]*$/, "Only following characters (A-Za-z0-9':,-_) are allowed in Apt./Unit/Suite."]
    },
    address: {
        type: String,
        // required: 'Please enter the city,state and country.'
    },
    zipCode: {
        type: String,
        // required: "Please enter the zip code."
    },
    phone: {
        type: String,
        // required: "Please enter the phone number."
    },
    planId: {
        type: String,
    },
    subscriptionId: {
        type: String,
        /*unique: true*/
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        // required: 'createdBy id is required.'
    },
    creatorType: {
        type: Number,
        enum: [1], //1-for superadmin
        // required: 'Document creator type is required.'
    },
    updatedOn: {
        type: Date
    },
    updatedBy: {
        type: Schema.Types.ObjectId
    },
    updaterType: {
        type: Number,
        enum: [1, 2] //1-superAdmin 2- admin
    },
    profileImage: {
        type: String
    },
    subscriptionActive: {
        type: Boolean,
        default: false
    },
    subType: {
        type: Number,
        enum: [1, 2] // 1- for manual subscription and 2 for braintree subsciption
    },
    fromDate: {
        type: Date
    },
    toDate: {
        type: Date
    }
}, {
    collection: 'companies'
});


companySchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

companySchema.statics.deserializeUser = function(obj, done) {
    done(null, obj);
};

// companySchema.plugin(uniqueValidator, {
//     message: "unique validation error"
// });


var companyObj = mongoose.model('companies', companySchema);
module.exports = companyObj;