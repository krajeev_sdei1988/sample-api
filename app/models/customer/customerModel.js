var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var customerSchema = new Schema({
    firstName: {
        type: String,
        required: 'First name is required',
    },
    lastName: {
        type: String,
        required: 'Last name is required'
    },
    email: {
        type: String,
        required: 'Please enter valid email id.',
        // match: [constantObj.emailFormat, 'Please enter a valid email address.'],
        lowercase: true
    },
    loginId: {
        type: Schema.Types.ObjectId,
        unique: true,
        required: 'loginId is required',
        ref: 'login'
    },
    companyId: {
        type: Schema.Types.ObjectId,
        required: 'Company ID is required.',
        ref: 'companies'
    },
    storeId: {
        type: Schema.Types.ObjectId,
        required: 'Store ID is required.',
        ref: 'stores'
    },
    userType: {
        type: Number,
        enum: [4],
        required: 'Please enter user type.' //1 -superadmin 2-admin 3-subAdmin 4-customer
    },
    notes: {
        type: String
    },
    landmark: {
        type: String,
    },
    streetNumber: {
        type: String,
        // required: 'Please enter the street number.'
    },
    address: {
        type: String,
        // required: 'Please enter the address.'
    },
    zipCode: {
        type: String,
        // required: 'PLease enter the zipcode.'
    },
    city: {
        type: String,
        // required: 'PLease enter the city.'
    },
    state: {
        type: String,
        // required: 'PLease enter the state.'
    },
    phoneHome: {
        type: String,
        // required: 'Home phone is required'
    },
    phoneMobile: {
        type: String,
        // required: 'Mobile phone is required'
    },
    phoneWork: {
        type: String,
        // required: 'Work phone is required'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        required: 'createdBy id is required.'
    },
    creatorType: {
        type: Number,
        enum: [1, 2, 3], //1-for superadmin
        required: 'Document creator type is required.'
    },
    updatedOn: {
        type: Date
    },
    updatedBy: {
        type: Schema.Types.ObjectId
    },
    updaterType: {
        type: Number,
        enum: [1, 2, 3] //1-superAdmin 2- admin
    },
    profileImage: {
        type: String
    }

}, {
    collection: 'customers'
});


customerSchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

customerSchema.statics.deserializeUser = function(obj, done) {
    done(null, obj);
};

var adminObj = mongoose.model('customers', customerSchema);
module.exports = adminObj;