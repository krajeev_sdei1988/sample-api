var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var specificationSchema = new Schema({
    brand: {
        type: String
    },
    cameraType: {
        type: String
    },
    lensType: {
        type: String
    },
    mountType: {
        type: String
    },
    lightingType: {
        type: String
    },
    lightingModifierType: {
        type: String
    },
    supportType: {
        type: String
    }
});

var productSchema = new Schema({
    productName: {
        type: String,
        required: 'Product Name is required.',
    },
    serialNumber: {
        type: String,
        // unique: true,
        // sparse: true,
        // required: 'Product serial number is required.'
    },
    rentalPrice: {
        type: Number
    },
    originalCost: {
        type: Number
    },
    oldCategory: {
        type: String
    },
    notes: {
        type: String
    },
    location: {
        type: String
    },
    oldInventoryId: {
        type: Number
    },
    datePurchased: {
        type: Date
    },
    description: {
        type: String
    },
    specifications:{
        type: specificationSchema
    },
    images: {
        type: Array
    },
    categoryId:{
        type: Schema.Types.ObjectId,
        ref: 'categories'
    },
    subCategoriesIds:{
        type: Array
    },
    companyId: {
        type: Schema.Types.ObjectId,
        required: 'CompanyId is required',
        ref: 'companies'
    },
    storeId: {
        type: Schema.Types.ObjectId,
        required: 'StoreId is required',
        ref: 'stores'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        required: 'createdBy id is required.'
    },
    creatorType: {
        type: Number,
        enum: [1, 2, 3], //1-for superadmin
        required: 'Document creator type is required.'
    },
    updatedOn: {
        type: Date
    },
    updatedBy: {
        type: Schema.Types.ObjectId
    },
    updaterType: {
        type: Number,
        enum: [1, 2, 3] //1-superAdmin 2- admin
    }
}, {
    collection: 'products'
});

productSchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

productSchema.statics.deserializeUser = function(obj, done) {
    done(null, obj);
};

var adminObj = mongoose.model('products', productSchema);
module.exports = adminObj;