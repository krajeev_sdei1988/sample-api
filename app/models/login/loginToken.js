var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var userTokenSchema = new mongoose.Schema({
  user: {
    type: Schema.Types.ObjectId
  },
  token: {
    type: String,
    required: true
  },
  userType: {
    type: Number,
    enum: [1,2,3,4],
    required: 'Please enter user type.' //1 -superadmin 2-admin 3-subAdmin 4-customer
  },
  expiredOn: {
    type: Date,
    default: Date.now
  },
  createdOn: {
    type: Date,
    default: Date.now()
  }
});

userTokenSchema.pre('save', function(next) {
  var token = this;
  var moment = require('moment');
  var date = new moment();
  date.add(60 * 60 * 24 * 1, 'seconds');
  token.expiredOn = date.toDate();
  next();
});

userTokenSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};
//custom validations
var userTokenObj = mongoose.model('userTokens', userTokenSchema);
module.exports = userTokenObj;