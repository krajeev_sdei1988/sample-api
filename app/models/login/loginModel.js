var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var superAdminObj = require('./../superAdmin/superAdminModel.js');
var constantObj = require('./../../../constants.js');

var loginSchema = new Schema({
    password: {
        type: String,
        required: 'Please enter password.'
    },
    email: {
        type: String,
        unique: true,
        required: 'Please enter valid email id.',
        // match: [constantObj.emailFormat, 'Please enter a valid email address.'],
        lowercase: true
    },
    userType: {
        type: Number,
        enum: [1, 2, 3, 4], // 1 - superAdmin :: 2 - admin :: 3 - subAdmin :: 4 - customer
        required: 'User type is required'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    createdBy: {
        type: Schema.Types.ObjectId
    },
    creatorType: {
        type: Number,
        enum: [1, 2, 3]
    },
    updaterType: {
        type: Number,
        enum: [1, 2, 3]
    },
    updatedOn: {
        type: Date,
    },
    updatedBy: {
        type: Schema.Types.ObjectId
    }
}, {
    collection: 'login'
});

loginSchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

loginSchema.statics.deserializeUser = function(obj, done) {
    var query = {
        loginId: obj._id,
        isDeleted: false
    }
    delete obj.password;
    var model = obj.userType == constantObj.userTypes.superAdmin ? superAdminObj : null;
    if (!model) return done(null, obj);
    if (obj.userType == constantObj.userTypes.superAdmin) {
        model.findOne(
            query
        ).exec(function(err, data) {
            if (err) {
                done(null, obj);
            } else {
                if (data) {
                    obj.userInfo = {};
                    obj.id = data._id;
                    obj.createdBy = data.createdBy;
                    obj.creatorType = data.creatorType;
                    obj.companyId = data.companyId != null ? data.companyId._id : null;
                    obj.profileImage = data.profileImage != null ? data.profileImage : null;
                    obj.companies = data.companies != null ? data.companies : null;
                    obj.subType = data.subType != null ? data.subType : null;
                    delete obj.status;
                    delete obj.isDeleted;
                    delete obj.__v;

                    obj.userInfo = {
                        "firstName": data.firstName,
                        "lastName": data.lastName,
                        "company": data.companyId != null ? data.companyId.cname : null
                    };
                }
            }
            done(null, obj);
        })
    } 
};

var loginModel = mongoose.model('login', loginSchema);
module.exports = loginModel;