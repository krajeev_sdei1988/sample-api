var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var subCategorySchema = new Schema({
    name: {
        type: String,
        required: 'Please enter the sub-category name.',
        match: [/^[A-Za-z0-9-' ]*$/, "Only following characters (a-z A-Z 0-9 - ') are allowed in company name."],
        min: [2, "Sub-Category Name is too short."],
        max: [30, "Sub-Category Name is too long."],
        unique: true
    },
    categoryId:{
        type: Schema.Types.ObjectId,
        unique: true,
        ref: 'categories'
    },
    companyId: {
        type: Schema.Types.ObjectId,
        unique: true,
        required: 'CompanyId is required',
        ref: 'companies'
    },
    storeId: {
        type: Schema.Types.ObjectId,
        unique: true,
        required: 'StoreId is required',
        ref: 'stores'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        // required: 'createdBy id is required.'
    },
    creatorType: {
        type: Number,
        enum: [1], //1-for superadmin
        // required: 'Document creator type is required.'
    },
    updatedOn: {
        type: Date
    },
    updatedBy: {
        type: Schema.Types.ObjectId
    },
    updaterType: {
        type: Number,
        enum: [1, 2] //1-superAdmin 2- admin
    }
}, {
    collection: 'categories'
});

subCategorySchema.statics.serializeUser = function(user, done) {
    done(null, user);
};

subCategorySchema.statics.deserializeUser = function(obj, done) {
    done(null, obj);
};

var subCatObj = mongoose.model('subCategories', subCategorySchema);
module.exports = subCatObj;