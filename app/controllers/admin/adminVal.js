const adminDbObj = require('./../../models/admin/adminModel.js');
const constantObj = require('./../../../constants.js');
const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
const CryptoJS = require("crypto-js");

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

// Validate the creteAdmin route
exports.createAdminVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body.password:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.passwordReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.firstName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.fisrtNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.lastName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.lastNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.email:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.emailReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.userType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.userTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.companyId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.companyId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.storeId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.storeId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.createdBy:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.createdBy):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.creatorType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.creatorTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}

// Validate the edit Admin route
exports.editAdminVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.firstName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.fisrtNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.lastName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.lastNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.userType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.userTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.companyId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.companyId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.storeId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.storeId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.createdBy:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.createdBy):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.creatorType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.creatorTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.updatedBy:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.updatedByReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.updatedBy):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.updatedByInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.updaterType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.updaterTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}

// Validate the list Admin route
exports.listAdminsVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body.companyId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.companyId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                // case !req.body.storeId:
                //     outputJson.status = constantObj.httpStatus.badRequest;
                //     outputJson.message = constantObj.messages.storeIdReq
                //     sendResponse(req, res, outputJson);
                //     break;
                // case !ObjectId.isValid(req.body.storeId):
                //     outputJson.status = constantObj.httpStatus.badRequest;
                //     outputJson.message = constantObj.messages.storeIdInvalid
                //     sendResponse(req, res, outputJson);
                //     break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}

// Validate the delete Admin route
exports.deleteAdminSoftVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}
// Validate the get Admin route
exports.getAdminDataVal = function(req, res){
    return function(req, res, next) {
        if(req.params){
            let outputJson = {}
            switch (true){
                case !req.params._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.params._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}

// Validate the get Admin route
exports.changeAdminStatusVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case typeof(req.body.status) === 'undefined' || typeof(req.body.status) === 'null' :
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.statusReq
                    sendResponse(req, res, outputJson);
                    break;
                case typeof(req.body.status) !== 'undefined' && typeof(req.body.status) !== 'null' && typeof(req.body.status) !== 'boolean' :
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.statusInvalid
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}