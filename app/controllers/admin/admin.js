const adminObj = require('./../../models/admin/adminModel.js');
const loginObj = require('./../../models/login/loginModel.js');
const constantObj = require('./../../../constants.js');
const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
const CryptoJS = require("crypto-js");
const middlewareObj = require('../../policies/auth.js');

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

async function newLoginCredentials(req, res){
    return new Promise((resolve, reject) => {
        let json = {};
        req.body.password ? json.password = CryptoJS.AES.encrypt(req.body.password, constantObj.cryptoSecret) : null;
        req.body.email ? json.email = req.body.email : null;
        req.body.userType ? json.userType = req.body.userType : null;
        req.body.createdBy ? json.createdBy = req.body.createdBy : null;
        req.body.creatorType ? json.creatorType = req.body.creatorType : null;
        let newLogin = new loginObj(json)
        newLogin.save((err, done) => {
            if(err) reject(err);
            else resolve(done);
        })
    })
}

async function newAdminDetails(req, res, loginData){
    return new Promise((resolve, reject) => {
        let json = {};
        req.body.firstName ? json.firstName = req.body.firstName : null;
        req.body.lastName ? json.lastName = req.body.lastName : null;
        req.body.email ? json.email = req.body.email : null;
        loginData && loginData._id ? json.loginId = loginData._id : null;
        req.body.companyId ? json.companyId = req.body.companyId : null;
        req.body.storeId ? json.storeId = req.body.storeId : null;
        req.body.userType ? json.userType = req.body.userType : null;
        req.body.createdBy ? json.createdBy = req.body.createdBy : null;
        req.body.creatorType ? json.creatorType = req.body.creatorType : null;
        let newAdmin = new adminObj(json)
        newAdmin.save((err, done) => {
            if(err) reject(err);
            else resolve(done);
        })
    })
}
// Create new Admins / Sub-Admins
exports.createAdmin = async function(req, res){
    let newLogin = await newLoginCredentials(req, res)
    .catch(e => {middlewareObj.errorHandler(req, res, e)})

    if(newLogin){
        let newAdmin = await newAdminDetails(req, res, newLogin)
        .catch(e => {middlewareObj.errorHandler(req, res, e)})
        if(newAdmin){
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = newAdmin
            sendResponse(req, res, outputJson);
        }
    }      
}

async function findOneAdmin(query){
    return new Promise((resolve, reject) => {
        adminObj.findOne(query).lean().exec((err, adminData) => { 
            if(err) reject(err)
            else{
                if(adminData) resolve(adminData);
                else reject({ 
                    status: constantObj.httpStatus.notFound, 
                    message: constantObj.messages.recordNotExist
                })
            }
        })
    })
}

async function updateAdmin(findQuery, setQuery, options = {}){
    return new Promise((resolve, reject) => {
        adminObj.update(findQuery, setQuery, options).exec((err, done) => { 
            if(err) reject(err)
            else{
                if(done) resolve(done);
                else reject({ 
                    status: constantObj.httpStatus.internalServerErr, 
                    message: constantObj.messages.genericErrMsg
                })
            }
        })
    })
}

exports.editAdmin = async function(req, res){
    let findQuery = {
        _id: req.body._id,
        isDeleted: false
    }
    let adminData = await findOneAdmin(findQuery).catch(e => {middlewareObj.errorHandler(req, res, e)})
    if(adminData){
        let body = req.body;
        if(req.user){
            body.updatedBy = req.user.id;
            body.updaterType = req.user.userType;
        }
        let setQuery = {
            $set: body
        }
        let updatedAdmin = await updateAdmin(findQuery, setQuery).catch(e => {middlewareObj.errorHandler(req, res, e)})
        if(updatedAdmin){
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = updatedAdmin
            sendResponse(req, res, outputJson);
        }
    }
}

async function findAdminsAgg(findQuery, page, pageLimit, skipNo, sortBy, sortType){
    return new Promise((resolve, reject) => {
        adminObj.aggregate(findQuery).exec((err, list) => {
            if(err) reject(err)
            else{
                if(list && list.length>0) resolve(list);
                else reject({ 
                    status: constantObj.httpStatus.notFound, 
                    message: constantObj.messages.recordNotExist
                })
            }
        })
    })
}

async function countAdmins(countQuery){
    return new Promise((resolve, reject) => {
        adminObj.aggregate(countQuery).exec((err, count) => {
            if(err) reject(err)
            else{
                if(count && count.length>0 && typeof(count[0].count) === 'number'){
                    resolve(count);
                } else resolve({ 
                    status: constantObj.httpStatus.notFound, 
                    message: constantObj.messages.recordNotExist
                })
            }
        })
    })
}

exports.listAdmins = async function(req, res){
    let page = parseInt(req.body.page) || 1;
    let pageLimit = parseInt(req.body.pageLimit) || 10;
    let skipNo = (page - 1) * pageLimit;
    let findQuery = [];
    let countQuery = [];
    let sortBy = req.body.sortBy || 'createdOn';
    let sortType = 1;
    if(req.body.sortType){
        if(req.body.sortType === 'asc')
            sortType = 1
        if(req.body.sortType === 'desc')
            sortType = -1
    }
    if(req.body.companyId){
        findQuery.push({
            $match :{
                companyId : mongoose.Types.ObjectId(req.body.companyId),
                isDeleted: false
            }
        });
        countQuery.push({
            $match :{
                companyId : mongoose.Types.ObjectId(req.body.companyId),
                isDeleted: false
            }
        });
    }
    if(req.body.search){
        let string = new RegExp(req.body.search, 'i');
        findQuery.push({
            $match: {
                $or: [{
                    'firstName': string
                }, {
                    'lastName': string
                }, {
                    'email': string
                }, {
                    'phone': string
                }]
            }
        })
        countQuery.push({
            $match: {
                $or: [{
                    'firstName': string
                }, {
                    'lastName': string
                }, {
                    'email': string
                }, {
                    'phone': string
                }]
            }
        })
    }

    countQuery.push({ 
        $group: { 
            _id: null, 
            count: { 
                $sum: 1 
            } 
        }
    })

    let sorting = {};
    sorting[sortBy] = parseInt(sortType)

    findQuery.push({
        $skip: parseInt(skipNo)
    }, {
        $limit: parseInt(pageLimit)
    }, {
        $sort: sorting
    })

    await Promise.all([findAdminsAgg(findQuery), countAdmins(countQuery)])
    .then((data) => {
        let outputJson = {};
        outputJson.status = constantObj.httpStatus.success;
        outputJson.data = data[0];
        outputJson.count = data[1][0].count;
        outputJson.message = constantObj.messages.dataFound
        sendResponse(req, res, outputJson);
    })
    .catch(e => {middlewareObj.errorHandler(req, res, e)});
}

exports.deleteAdminSoft = async function(req, res){
    let findQuery = {
        _id: req.body._id,
        isDeleted: false
    }
    let adminPresent = await findOneAdmin(findQuery).catch(e => {middlewareObj.errorHandler(req, res, e)});
    if(adminPresent){
        let setQuery = {};
        setQuery.isDeleted = true;
        if(req.user){
            setQuery.updatedBy = req.user.id;
            setQuery.updaterType = req.user.userType;
        }

        let deleted = await updateAdmin(findQuery, setQuery)
        .then((data) => {
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = constantObj.messages.userDeleted;
            sendResponse(req, res, outputJson);
        })
        .catch(e => {middlewareObj.errorHandler(req, res, e)})
    }
}

exports.getAdminData = async function(req, res){
    let findQuery = {};
    findQuery._id = req.params._id;

    await findOneAdmin(findQuery)
    .then((data) => {
        let outputJson = {};
        outputJson.status = constantObj.httpStatus.success;
        outputJson.message = constantObj.messages.userFound;
        outputJson.data = data;
        sendResponse(req, res, outputJson);
    })
    .catch(e => {middlewareObj.errorHandler(req, res, e)});
}

exports.changeAdminStatus = async function(req, res){
    let findQuery = {};
    findQuery._id = req.body._id;
    let adminPresent = await findOneAdmin(findQuery).catch(e => {middlewareObj.errorHandler(req, res, e)});
    if(adminPresent){
        let setQuery = {};
        setQuery.status = req.body.status;
        if(req.user){
            setQuery.updatedBy = req.user.id;
            setQuery.updaterType = req.user.userType;
        }

        let status = await updateAdmin(findQuery, setQuery)
        .then((data) => {
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = constantObj.messages.statusChanged;
            sendResponse(req, res, outputJson);
        })
        .catch(e => {middlewareObj.errorHandler(req, res, e)})
    }
}