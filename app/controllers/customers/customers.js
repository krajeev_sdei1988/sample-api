const adminObj = require('./../../models/admin/adminModel.js');
const loginObj = require('./../../models/login/loginModel.js');
const constantObj = require('./../../../constants.js');
const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
const CryptoJS = require("crypto-js");
const middlewareObj = require('../../policies/auth.js');
const randomstring = require("randomstring");
const nodemailer = require('nodemailer');

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

// Create new customer from register form.
async function newLoginCredentials(req, res){
    return new Promise((resolve, reject) => {
        let json = {};
        req.body.password ? json.password = CryptoJS.AES.encrypt(req.body.password, constantObj.cryptoSecret) : null;
        req.body.email ? json.email = req.body.email : null;
        req.body.userType ? json.userType = req.body.userType : null;
        req.body.createdBy ? json.createdBy = req.body.createdBy : null;
        req.body.creatorType ? json.creatorType = req.body.creatorType : null;
        let newLogin = new loginObj(json)
        newLogin.save((err, done) => {
            if(err) reject(err);
            else {
                resolve(done);
            }
        })
    })
}

// Create new customer from Admin Portal.
async function newLoginCredentialsEmail(req, res){
    return new Promise((resolve, reject) => {
        let json = {};
        let random = randomstring.generate({
            length: 8,
            charset: 'alphabetic'
        });
        json.password = CryptoJS.AES.encrypt(random, constantObj.cryptoSecret);
        req.body.email ? json.email = req.body.email : null;
        req.body.userType ? json.userType = req.body.userType : null;
        req.body.createdBy ? json.createdBy = req.body.createdBy : null;
        req.body.creatorType ? json.creatorType = req.body.creatorType : null;
        let newLogin = new loginObj(json)
        newLogin.save((err, done) => {
            if(err) reject(err);
            else {
                done.origPass = random;
                resolve(done);
            }
        })
    })
}

async function newCustomerDetails(req, res, loginData){
    return new Promise((resolve, reject) => {
        let json = {};
        req.body.firstName ? json.firstName = req.body.firstName : null;
        req.body.lastName ? json.lastName = req.body.lastName : null;
        req.body.email ? json.email = req.body.email : null;
        loginData && loginData._id ? json.loginId = loginData._id : null;
        req.body.companyId ? json.companyId = req.body.companyId : null;
        req.body.storeId ? json.storeId = req.body.storeId : null;
        req.body.userType ? json.userType = req.body.userType : null;
        req.body.createdBy ? json.createdBy = req.body.createdBy : null;
        req.body.creatorType ? json.creatorType = req.body.creatorType : null;
        let newAdmin = new adminObj(json)
        newAdmin.save((err, done) => {
            if(err) reject(err);
            else resolve(done);
        })
    })
}

async function sendEmailPassword(req, res, loginData){
    return new Promise((resolve, reject) => {
        let transporter = nodemailer.createTransport({
            service: "gmail",
            host: "smtp.gmail.com",
            auth: {
                user: "abhishek.sdei17@gmail.com",
                pass: "abhishekSDEI1"
            }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: 'abhishek.sdei17@gmail.com', // sender address
            to: loginData.email, // list of receivers
            subject: 'Hello ✔', // Subject line
            text: 'Hello New Member?', // plain text body
            html: '<p><b>Hello New MEmber</b></p><p>Your password is: ' + loginData.origPass  // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                reject(error)
            }else{
                resolve(info)
            }
        });
    })
}

// Create new customer from admin panel
exports.createCustomerAdmin = async function(req, res){
    let newLogin = await newLoginCredentialsEmail(req, res)
    .catch(e => {middlewareObj.errorHandler(req, res, e)})

    if(newLogin){
        let newCust = Promise.settle([sendEmailPassword(req, res, newLogin), newCustomerDetails(req, res, newLogin)])
        .then((done) => {
            let outputJson = {}
            let rejections = done.filter(el => { return el.isRejected(); });
            if(rejections && rejections.length>0){
                let errors = [];
                rejections.filter(x => {
                    errors.push({error: x.reason()});
                })
                outputJson.status = constantObj.httpStatus.badRequest;
                outputJson.error = errors;
                sendResponse(req, res, outputJson);
            }else{
                outputJson.status = constantObj.httpStatus.success;
                outputJson.message = constantObj.messages.customerAdded;
                sendResponse(req, res, outputJson);
            }
        })
    }      
}

// Create new customer from register form.
exports.createCustomer = async function(req, res){
    let newLogin = await newLoginCredentials(req, res)
    .catch(e => {middlewareObj.errorHandler(req, res, e)})

    if(newLogin){
        let newCust = await newCustomerDetails(req, res, newLogin)
        .then((data) => {
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = constantObj.messages.customerAdded
            sendResponse(req, res, outputJson);
        })
        .catch(e => {middlewareObj.errorHandler(req, res, e)});
    }      
}