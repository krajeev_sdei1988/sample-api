const loginObj = require('./../../models/login/loginModel.js');
const constantObj = require('./../../../constants.js');
const customerObj = require('./../../models/customer/customerModel.js');
const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
const middlewareObj = require('../../policies/auth.js');
const xlsxj = require("xlsx-to-json");
const formidable = require('formidable');

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

// Validate the crete customer route
exports.createCustomerAdminVal = function(fromRegister = false){
    return function(req, res, next) {
        console.log("-->",fromRegister)
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body.firstName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.fisrtNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.lastName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.lastNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.email:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.emailReq
                    sendResponse(req, res, outputJson);
                    break;
                case fromRegister && !req.body.password:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.passwordReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.userType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.userTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                case req.body.userType && req.body.userType !== constantObj.userTypes.customer:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.userTypeInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.companyId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.companyId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.storeId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.storeId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.createdBy:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.createdBy):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.creatorType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.creatorTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}