const superAdminDbObj = require('./../../models/superAdmin/superAdminModel.js');
const adminObj = require('./../../models/admin/adminModel.js');
const customerObj = require('./../../models/customer/customerModel.js');
const constantObj = require('./../../../constants.js');
const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const CryptoJS = require("crypto-js");

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

function loadUserData(req, res){
    let model;
    switch (req.user.userType){
        case constantObj.userTypes.superAdmin: 
            model = superAdminDbObj;
            break;
        case constantObj.userTypes.admin: 
            model = adminObj;
            break;
        case constantObj.userTypes.subAdmin: 
            model = adminObj;
            break;
        case constantObj.userTypes.customer:
            model = customerObj;
            break;
        default: 
            model = null
            break;
    }

    if(model){
        let query ={
            loginId: mongoose.Types.ObjectId(req.user._id),
            isDeleted: false
        }
        model.findOne(query).exec((err, userData) => {
            if(err) {
                let outputJson = {
                    status: 400,
                    message: err,
                }
                sendResponse(req, res, outputJson)
            }
            else{
                if(userData){
                    let token = req.user.myToken
                    req.user = {};
                    req.user.myToken = token || null;
					req.user.id = userData._id
					req.user.firstName = userData.firstName != null ? userData.firstName : null;
					req.user.lastName = userData.lastName != null ? userData.lastName : null;
					req.user.companyName = userData.cname != null ? userData.cname : null;
					req.user.companyId = userData.companyId != null ? userData.companyId : null;
					req.user.storeId = userData.storeId != null ? userData.storeId : null;
					req.user.landmark = userData.landmark != null ? userData.landmark : null;
					req.user.streetNumber = userData.streetNumber != null ? userData.streetNumber : null;
					req.user.address = userData.address != null ? userData.address : null;
					req.user.zipCode = userData.zipCode != null ? userData.zipCode : null;
					req.user.phone = userData.phone != null ? userData.zipCode : null;;
					req.user.profileImage = userData.profileImage != null ? userData.profileImage : null;
                    req.user.loginId = userData.loginId != null ? userData.loginId : null;
                    req.user.userType = userData.userType != null ? userData.userType : null;
					let outputJson = {
						status: 200,
						message: constantObj.messages.userLoggedInOk,
						userInfo: req.user
                    }
                    sendResponse(req, res, outputJson)
                }else{
                    let outputJson = {
						status: constantObj.httpStatus.unauthorized,
						message: constantObj.messages.noProfileEr,
                    }
                    sendResponse(req, res, outputJson)
                }
            }
        })
    }
}

exports.authenticate = function(req, res){
    if(req.user){
        if(!req.user.status){
            return res.status(constantObj.httpStatus.unauthorized).jsonp({
                status: constantObj.httpStatus.unauthorized,
                message: req.user.message
            });
        }else{
            loadUserData(req, res);
        }
    }
}