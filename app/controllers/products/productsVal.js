const adminDbObj = require('./../../models/product/productModel.js');
const constantObj = require('./../../../constants.js');
const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
const CryptoJS = require("crypto-js");

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

exports.listProductsVal = function(req, res){
    return function(req, res, next){
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body.companyId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.companyId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.storeId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.storeId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}


// Validate the create product route
exports.createProductVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body.productName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.productNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.serialNumber:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.serialNumberReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.rentalPrice:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.rentalPriceReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.originalCost:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.originalCostReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.companyId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.companyId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.storeId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.storeId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.createdBy:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.createdBy):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.creatorType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.creatorTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}

// Validate the edit product route
exports.editProductVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                    case !req.body.productName:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.productNameReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.serialNumber:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.serialNumberReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.rentalPrice:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.rentalPriceReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.originalCost:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.originalCostReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.companyId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.companyId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.companyIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.storeId:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.storeId):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.storeIdInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.createdBy:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.createdBy):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.createdByInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.creatorType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.creatorTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.updatedBy:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.updatedByReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body.updatedBy):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.updatedByInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case !req.body.updaterType:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.updaterTypeReq
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}

// Validate the delete Product route
exports.deleteProductSoftVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}
// Validate the get Product route
exports.getProductDataVal = function(req, res){
    return function(req, res, next) {
        if(req.params){
            let outputJson = {}
            switch (true){
                case !req.params._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.params._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}

// Validate the get Product route
exports.changeProductStatusVal = function(req, res){
    return function(req, res, next) {
        if(req.body){
            let outputJson = {}
            switch (true){
                case !req.body._id:
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idReq
                    sendResponse(req, res, outputJson);
                    break;
                case !ObjectId.isValid(req.body._id):
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.idInvalid
                    sendResponse(req, res, outputJson);
                    break;
                case typeof(req.body.status) === 'undefined' || typeof(req.body.status) === 'null' :
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.statusReq
                    sendResponse(req, res, outputJson);
                    break;
                case typeof(req.body.status) !== 'undefined' && typeof(req.body.status) !== 'null' && typeof(req.body.status) !== 'boolean' :
                    outputJson.status = constantObj.httpStatus.badRequest;
                    outputJson.message = constantObj.messages.statusInvalid
                    sendResponse(req, res, outputJson);
                    break;
                default:
                    next();
            }
        } else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.noReqData
            sendResponse(req, res, outputJson);
        }
    }
}