const loginObj = require('./../../models/login/loginModel.js');
const constantObj = require('./../../../constants.js');
const productObj = require('./../../models/product/productModel.js');
const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
const middlewareObj = require('../../policies/auth.js');
const xlsxj = require("xlsx-to-json");
const formidable = require('formidable');

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

async function getPromises(req, res, fields, files, result){
    let promiseArr = [];
    result.map((x) => {
        // return new Promise((resolve, reject) => {
            x.createdBy = fields.createdBy;
            x.creatorType = fields.creatorType;
            x.companyId = fields.companyId;
            x.storeId = fields.storeId;
            // x.serialNumber = x.serialNumber || 'n/a';
            let newProduct = new productObj(x);
            promiseArr.push(newProduct.save());
        // })
    })
    return promiseArr;
}

async function uploadProducts(req, res, fields, files, result){
    let promises = await getPromises(req, res, fields, files, result);
    let process = Promise.settle(promises)
    .then((done) => {
        let outputJson = {}
        let rejections = done.filter(el => { return el.isRejected(); });
        if(rejections && rejections.length>0){
            let errors = [];
            rejections.filter(x => {
                errors.push({error: x.reason()});
            })
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.companyIdReq;
            outputJson.error = errors;
            sendResponse(req, res, outputJson);
        }else{
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = constantObj.messages.productAdded;
            sendResponse(req, res, outputJson);
        }
    })
}

exports.convertProducts = async function(req, res){
	let form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        if(err){
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = constantObj.messages.invalidForm;
            sendResponse(req, res, outputJson);
        }else{
            if(fields){
                let outputJson = {}
                switch (true){
                    case !fields.companyId:
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.companyIdReq
                        sendResponse(req, res, outputJson);
                        break;
                    case !ObjectId.isValid(fields.companyId):
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.companyIdInvalid
                        sendResponse(req, res, outputJson);
                        break;
                    case !fields.storeId:
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.storeIdReq
                        sendResponse(req, res, outputJson);
                        break;
                    case !ObjectId.isValid(fields.storeId):
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.storeIdInvalid
                        sendResponse(req, res, outputJson);
                        break;
                    case !fields.createdBy:
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.createdByReq
                        sendResponse(req, res, outputJson);
                        break;
                    case !ObjectId.isValid(fields.createdBy):
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.createdByInvalid
                        sendResponse(req, res, outputJson);
                        break;
                    case !fields.creatorType:
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.creatorTypeReq
                        sendResponse(req, res, outputJson);
                        break;
                    case !files || !files.excel || !files.excel.path:
                        outputJson.status = constantObj.httpStatus.badRequest;
                        outputJson.message = constantObj.messages.uploadedFileNotFound
                        sendResponse(req, res, outputJson);
                    default:
                        xlsxj({
                            input: files.excel.path, 
                            output: "uploads/output.json"
                        }, function(err, result) {
                            if(err) console.log("err--->", err)
                            else {
                                console.log("file converted")
                                uploadProducts(req, res, fields, files, result);
                                // populateCollection(req, res, fields, files, result)
                            }
                        });
                }
            } else{
                let outputJson = {};
                outputJson.status = constantObj.httpStatus.badRequest;
                outputJson.message = constantObj.messages.invalidForm;
                sendResponse(req, res, outputJson);
            }
        }
    })
}

async function findOneProduct(query){
    return new Promise((resolve, reject) => {
        productObj.findOne(query).lean().exec((err, prodData) => { 
            if(err) reject(err)
            else{
                if(prodData) resolve(prodData);
                else reject({ 
                    status: constantObj.httpStatus.notFound, 
                    message: constantObj.messages.recordNotExist
                })
            }
        })
    })
}

// For listing the products:

async function findProductsAgg(findQuery, page, pageLimit, skipNo, sortBy, sortType){
    return new Promise((resolve, reject) => {
        productObj.aggregate(findQuery).exec((err, list) => {
            if(err) reject(err)
            else{
                if(list && list.length>0) resolve(list);
                else reject({ 
                    status: constantObj.httpStatus.notFound, 
                    message: constantObj.messages.recordNotExist
                })
            }
        })
    })
}

async function countProducts(countQuery){
    return new Promise((resolve, reject) => {
        productObj.aggregate(countQuery).exec((err, count) => {
            if(err) reject(err)
            else{
                if(count && count.length>0 && typeof(count[0].count) === 'number'){
                    resolve(count);
                } else reject({ 
                    status: constantObj.httpStatus.notFound, 
                    message: constantObj.messages.recordNotExist
                })
            }
        })
    })
}

exports.listProducts = async function(req, res){
    let page = parseInt(req.body.page) || 1;
    let pageLimit = parseInt(req.body.pageLimit) || 10;
    let skipNo = (page - 1) * pageLimit;
    let findQuery = [];
    let countQuery = [];
    let sortBy = req.body.sortBy || 'createdOn';
    let sortType = 1;
    if(req.body.sortType){
        if(req.body.sortType === 'asc')
            sortType = 1
        if(req.body.sortType === 'desc')
            sortType = -1
    }
    if(req.body.companyId){
        findQuery.push({
            $match :{
                companyId : mongoose.Types.ObjectId(req.body.companyId),
                storeId : mongoose.Types.ObjectId(req.body.storeId),
                isDeleted: false
            }
        });
        countQuery.push({
            $match :{
                companyId : mongoose.Types.ObjectId(req.body.companyId),
                storeId : mongoose.Types.ObjectId(req.body.storeId),
                isDeleted: false
            }
        });
    }
    if(req.body.search){
        let string = new RegExp(req.body.search, 'i');
        findQuery.push({
            $match: {
                $or: [{
                    'productName': string
                }, {
                    'serialNumber': string
                }, {
                    'notes': string
                }]
            }
        })
        countQuery.push({
            $match: {
                $or: [{
                    'productName': string
                }, {
                    'serialNumber': string
                }, {
                    'notes': string
                }]
            }
        })
    }

    countQuery.push({ 
        $group: { 
            _id: null, 
            count: { 
                $sum: 1 
            } 
        }
    })

    let sorting = {};
    sorting[sortBy] = parseInt(sortType)

    findQuery.push({
        $skip: parseInt(skipNo)
    }, {
        $limit: parseInt(pageLimit)
    }, {
        $sort: sorting
    })

    let products = await Promise.all([findProductsAgg(findQuery), countProducts(countQuery)])
    .then((data) => {
        let outputJson = {};
        outputJson.status = constantObj.httpStatus.success;
        outputJson.data = data[0];
        outputJson.count = data[1][0].count;
        outputJson.message = constantObj.messages.dataFound
        sendResponse(req, res, outputJson);
    })
    .catch(e => {middlewareObj.errorHandler(req, res, e)});
}

// create product API

async function newProduct(req, res){
    return new Promise((resolve, reject) => {
        let json = {};
        req.body.productName ? json.productName = req.body.productName : null;
        req.body.serialNumber ? json.serialNumber = req.body.serialNumber : null;
        req.body.rentalPrice ? json.rentalPrice = req.body.rentalPrice : null;
        req.body.originalCost ? json.originalCost = req.body.originalCost : null;
        req.body.notes ? json.notes = req.body.notes : null;
        req.body.datePurchased ? json.datePurchased = req.body.datePurchased : null;
        req.body.categoryId ? json.categoryId = req.body.categoryId : null;
        req.body.subCategoriesIds ? json.subCategoriesIds = req.body.subCategoriesIds : null;
        req.body.companyId ? json.companyId = req.body.companyId : null;
        req.body.storeId ? json.storeId = req.body.storeId : null;
        req.body.createdBy ? json.createdBy = req.body.createdBy : null;
        req.body.creatorType ? json.creatorType = req.body.creatorType : null;
        req.body.specifications ? json.specifications = req.body.specifications : null;
        let newItems = new productObj(json);
        newItems.save((err, done) => {
            if(err) {console.log(err);reject(err)}
            else resolve(done);
        })
    })
}

exports.createProduct = async function(req, res){
    let newItem = await newProduct(req, res)
    .then((done) => {
        let outputJson = {};
        outputJson.status = constantObj.httpStatus.success;
        outputJson.message = done
        sendResponse(req, res, outputJson);
    })
    .catch(e => {middlewareObj.errorHandler(req, res, e)})  
}

// Edit products

async function updateProduct(findQuery, setQuery, options = {}){
    return new Promise((resolve, reject) => {
        productObj.update(findQuery, setQuery, options).exec((err, done) => { 
            if(err) reject(err)
            else{
                if(done) resolve(done);
                else reject({ 
                    status: constantObj.httpStatus.internalServerErr, 
                    message: constantObj.messages.genericErrMsg
                })
            }
        })
    })
}

exports.editProduct = async function(req, res){
    let findQuery = {
        _id: req.body._id,
        isDeleted: false
    }
    let prodData = await findOneProduct(findQuery).catch(e => {middlewareObj.errorHandler(req, res, e)})
    if(prodData){
        let body = req.body;
        if(req.user){
            body.updatedBy = req.user.id;
            body.updaterType = req.user.userType;
        }
        let setQuery = {
            $set: body
        }
        let updatedProd = await updateProduct(findQuery, setQuery).catch(e => {middlewareObj.errorHandler(req, res, e)})
        if(updatedProd){
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = updatedProd
            sendResponse(req, res, outputJson);
        }
    }
}

// Delete specific Product
exports.deleteProductSoft = async function(req, res){
    let findQuery = {
        _id: req.body._id,
        isDeleted: false
    }
    let productPresent = await findOneProduct(findQuery).catch(e => {middlewareObj.errorHandler(req, res, e)});
    if(productPresent){
        let setQuery = {};
        setQuery.isDeleted = true;
        if(req.user){
            setQuery.updatedBy = req.user.id;
            setQuery.updaterType = req.user.userType;
        }

        let deleted = await updateProduct(findQuery, setQuery)
        .then((data) => {
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = constantObj.messages.productDeleted;
            sendResponse(req, res, outputJson);
        })
        .catch(e => {middlewareObj.errorHandler(req, res, e)})
    }
}

// Get specific product data
exports.getProductData = async function(req, res){
    let findQuery = {};
    findQuery._id = req.params._id;

    await findOneProduct(findQuery)
    .then((data) => {
        let outputJson = {};
        outputJson.status = constantObj.httpStatus.success;
        outputJson.message = constantObj.messages.productFound;
        outputJson.data = data;
        sendResponse(req, res, outputJson);
    })
    .catch(e => {middlewareObj.errorHandler(req, res, e)});
}

// Change product status
exports.changeProductStatus = async function(req, res){
    let findQuery = {};
    findQuery._id = req.body._id;
    let productPresent = await findOneProduct(findQuery).catch(e => {middlewareObj.errorHandler(req, res, e)});
    if(productPresent){
        let setQuery = {};
        setQuery.status = req.body.status;
        if(req.user){
            setQuery.updatedBy = req.user.id;
            setQuery.updaterType = req.user.userType;
        }

        let status = await updateProduct(findQuery, setQuery)
        .then((data) => {
            let outputJson = {};
            outputJson.status = constantObj.httpStatus.success;
            outputJson.message = constantObj.messages.statusChanged;
            sendResponse(req, res, outputJson);
        })
        .catch(e => {middlewareObj.errorHandler(req, res, e)})
    }
}