const loginObj = require('../models/login/loginModel.js')
const superAdminObj = require('../models/superAdmin/superAdminModel.js');
const adminObj = require('../models/admin/adminModel.js');
const companyObj = require('../models/company/companyModel.js');
const storeObj = require('../models/store/storeModel.js');
const CryptoJS = require("crypto-js");
const constantObj = require('../../constants.js');
const xlsxj = require("xlsx-to-json");
const formidable = require('formidable');
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

function customErrorHandler(e){
    let outputJson = {};
    if(e && e.name === 'MongoError' && e.code == 11000){
        var regex = /index\:\ (?:.*\.)?\$?(?:([_a-z0-9]*)(?:_\d*)|([_a-z0-9]*))\s*dup key/i,      
        match =  e.message.match(regex),  
        indexName = match[1] || match[2];  
        
        outputJson.status = 400;
        outputJson.message = indexName+ " already present in database."
    }else{
        outputJson.status = 400;
        outputJson.message = e
    } 
}

async function checkSuperCount(){
	return new Promise((resolve, reject) => {
		superAdminObj.count().exec((err, count) => {
			if(err) reject(err);
			else resolve(count);
		})
	})
}

async function newSuperLogin(){
	return new Promise((resolve, reject) => {
		let json = {
			email: "chris.super@yopmail.com",
			password: CryptoJS.AES.encrypt('123456', constantObj.cryptoSecret),
			userType: 1
		}
		var newSuperLogin = new loginObj(json);
		newSuperLogin.save((err, loginData) => {
			if(err) reject(err);
			else resolve(loginData);
		})
	})
}

async function newSuperAdmin(superLoginData){
	return new Promise((resolve, reject) => {
		let json = {
			firstName: "Chris",
			lastName: "Maxwell",
			email: "chris.super@yopmail.com",
			userType: 1,
			loginId: superLoginData._id
		}
		let newSuperUser = new superAdminObj(json);
		newSuperUser.save((err, superAdminData) => {
			if(err) reject(err);
			else resolve(superAdminData);
		})
	})
}

async function newCompany(superAdminData){
	return new Promise((resolve, reject) => {
		let json = {
			name: "sample",
			website: 'www.sample.com',
			routeUrl: 'sample',
			createdBy: superAdminData._id,
			creatorType: superAdminData.userType
		}
		var newCompany = new companyObj(json);
		newCompany.save((err, companyData) => {
			if(err) reject(err);
			else resolve(companyData);
		})
	})
}

async function newStore(superAdminData, companyData){
	return new Promise((resolve, reject) => {
		let json = {
			name: "sample - Salt lake City",
			website: 'www.sample.com',
			companyId: companyData._id,
			createdBy: superAdminData._id,
			creatorType: superAdminData.userType
		}
		var newStore = new storeObj(json);
		newStore.save((err, storeData) => {
			if(err) reject(err);
			else resolve(storeData);
		})
	})
}

async function newAdminLogin(superAdminData){
	return new Promise((resolve, reject) => {
		let json = {
			email: "chris.admin@yopmail.com",
			password: CryptoJS.AES.encrypt('123456', constantObj.cryptoSecret),
			userType: 2,
			createdBy: superAdminData._id,
			creatorType: superAdminData.userType
		}
		let newAdminLogin = new loginObj(json);
		newAdminLogin.save((err, adminLoginData) => {
			if(err) reject(err);
			else resolve(adminLoginData);
		})
	})
}

async function newAdmin(adminLoginData, companyData, storeData, superAdminData){
	return new Promise((resolve, reject) => {
		let adminUserData = {
			firstName: "Chris",
			lastName: "Maxwell",
			email: "chris.admin@yopmail.com",
			userType: 2,
			loginId: adminLoginData._id,
			companyId: companyData._id,
			storeId: storeData._id,
			createdBy: superAdminData._id,
			creatorType: superAdminData.userType
		}
		var newAdminUser = new adminObj(adminUserData);
		newAdminUser.save((err, adminData) => {
			if(err) reject(err);
			else resolve(adminData);
		})
	})
}

exports.populateDB = async function(){
    let superAdminCount = await checkSuperCount().catch(e =>{customErrorHandler(e)});
    if(superAdminCount === 0){
        let superLogin = await newSuperLogin().catch(e =>{customErrorHandler(e)});
        let superAdminData = await newSuperAdmin(superLogin).catch(e =>{customErrorHandler(e)});
        let companyData = await newCompany(superAdminData).catch(e =>{customErrorHandler(e)});
        let storeData = await newStore(superAdminData, companyData).catch(e =>{customErrorHandler(e)});
        let adminLogin = await newAdminLogin(superAdminData).catch(e =>{customErrorHandler(e)});
        let adminData = await newAdmin(adminLogin, companyData, storeData, superAdminData).catch(e =>{customErrorHandler(e)});
		console.log("DB has been populated....");
    } else{
        console.log("Super Admin already present....");
    }
}