const loginObj = require('../models/login/loginModel.js')
const superAdminObj = require('../models/superAdmin/superAdminModel.js');
const adminObj = require('../models/admin/adminModel.js');
const companyObj = require('../models/company/companyModel.js');
const storeObj = require('../models/store/storeModel.js');
const CryptoJS = require("crypto-js");
const constantObj = require('../../constants.js')
const xlsxj = require("xlsx-to-json");

exports.uploadProducts = function(){
    xlsxj({
        input: "uploads/sample_products.xlsx", 
        output: "uploads/output.json"
    }, function(err, result) {
        if(err) {
          console.error(err);
        }else {
          console.log(result);
        }
    });
}