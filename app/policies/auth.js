var constantObj = require('./../../constants.js');
var userTokenObj = require('./../models/login/loginToken.js');
var tokenService = require('./../services/authToken.js');
const loginObj = require('../models/login/loginModel.js')
const superAdminObj = require('../models/superAdmin/superAdminModel.js');
const adminObj = require('../models/admin/adminModel.js');
const companyObj = require('../models/company/companyModel.js');
const customerObj = require('../models/customer/customerModel.js');
var mongoose = require('mongoose');
var formidable = require('formidable');
var fs = require('fs');

function sendResponse(req, res, outputJson) {
    try {
        return res.status(outputJson.status).jsonp(outputJson);
    } catch (e) {
        // console.log("e:", e);
    }
}

exports.errorHandler = function(req, res, e){
    let outputJson = {};
    if(e.status){
        outputJson.status = e.status;
        outputJson.message = e.message;
        sendResponse(req, res, outputJson);
    }else{
        if(e && e.name === 'MongoError' && e.code == 11000){
            var regex = /index\:\ (?:.*\.)?\$?(?:([_a-z0-9]*)(?:_\d*)|([_a-z0-9]*))\s*dup key/i,      
            match =  e.message.match(regex),  
            indexName = match[1] || match[2];  
            
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = indexName+ " already present in database."
            sendResponse(req, res, outputJson);
        }else{
            outputJson.status = constantObj.httpStatus.badRequest;
            outputJson.message = e
            sendResponse(req, res, outputJson);
        }
    }
}

//checkToken middleware is used for checking the login bearer token for api hit authentication 
exports.checkToken = function() {
    return function(req, res, next) {
        let token = req.header('Authorization');
        if (!token) {
            return res.status(constantObj.httpStatus.unauthorized).json({
                status: constantObj.httpStatus.unauthorized,
                message: "Authorization token required"
            })
        } else {
            var checkToken = token.split(' ');
            tokenService.verifyToken(checkToken[1], function(verErr, verOk) {
                if (verErr) {
                    return res.status(constantObj.httpStatus.unauthorized).json({
                        status: constantObj.httpStatus.unauthorized,
                        message: "Token verification failed: " + verErr.message
                    })
                } else {
                    userTokenObj.findOne({
                        token: checkToken[1]
                    }, function(err, data) {
                        if (err) {
                            return res.status(constantObj.httpStatus.unauthorized).json({
                                "status": constantObj.httpStatus.unauthorized,
                                message: constantObj.messages.NotAuthenticated
                            })
                        } else {
                            if (data) {
                                if (data.token === checkToken[1]) {
                                    req.loggedUser = {};
                                    req.loggedUser.loginId = data.user;
                                    req.loggedUser.userType = data.userType;
                                    let model;
                                    if(data.userType === constantObj.userTypes.superAdmin)
                                        model = superAdminObj;
                                    if(data.userType === constantObj.userTypes.admin)
                                        model = adminObj;
                                    if(data.userType === constantObj.userTypes.subAdmin)
                                        model = adminObj;
                                    if(data.userType === constantObj.userTypes.customer)
                                        model = customerObj

                                    model.findOne({loginId: data.user}).exec((err, userData) => {
                                        if(err){
                                            return res.status(constantObj.httpStatus.unauthorized).json({
                                                status: constantObj.httpStatus.unauthorized,
                                                message: constantObj.messages.NotAuthenticated
                                            })
                                        }else{
                                            if(userData){
                                                let token = data.token;
                                                req.user = {};
                                                req.user.myToken = token || null;
                                                req.user.id = userData._id
                                                req.user.firstName = userData.firstName != null ? userData.firstName : null;
                                                req.user.lastName = userData.lastName != null ? userData.lastName : null;
                                                req.user.companyName = userData.cname != null ? userData.cname : null;
                                                req.user.companyId = userData.companyId != null ? userData.companyId : null;
                                                req.user.storeId = userData.storeId != null ? userData.storeId : null;
                                                req.user.landmark = userData.landmark != null ? userData.landmark : null;
                                                req.user.streetNumber = userData.streetNumber != null ? userData.streetNumber : null;
                                                req.user.address = userData.address != null ? userData.address : null;
                                                req.user.zipCode = userData.zipCode != null ? userData.zipCode : null;
                                                req.user.phone = userData.phone != null ? userData.zipCode : null;;
                                                req.user.profileImage = userData.profileImage != null ? userData.profileImage : null;
                                                req.user.loginId = userData.loginId != null ? userData.loginId : null;
                                                req.user.userType = userData.userType != null ? userData.userType : null;
                                                next();
                                            }else{
                                                return res.status(constantObj.httpStatus.unauthorized).json({
                                                    status: constantObj.httpStatus.unauthorized,
                                                    message: constantObj.messages.NotAuthenticated
                                                })
                                            }
                                        }
                                    })
                                } else {
                                    return res.status(constantObj.httpStatus.unauthorized).json({
                                        status: constantObj.httpStatus.unauthorized,
                                        message: constantObj.messages.NotAuthenticated
                                    })
                                }
                            } else {
                                return res.status(constantObj.httpStatus.unauthorized).json({
                                    status: constantObj.httpStatus.unauthorized,
                                    message: constantObj.messages.NotAuthenticated
                                })
                            }
                        }
                    })
                }
            })
        }
    }
}

exports.isSupAdmSub = function(){
    return function(req, res, next){
        let a = [1,2,3];
        if(a.indexOf(req.user.userType) !== -1){
            next();
        }else{
            return res.status(constantObj.httpStatus.forbidden).jsonp({
                status: constantObj.httpStatus.forbidden,
                message: 'User not authorized'
            });
        }
    }
}

exports.isSupAdm = function(){
    return function(req, res, next){
        let a = [1,2];
        if(a.indexOf(req.user.userType) !== -1){
            next();
        }else{
            return res.status(constantObj.httpStatus.forbidden).jsonp({
                status: constantObj.httpStatus.forbidden,
                message: 'User not authorized'
            });
        }
    }
}

exports.isSuperAdmin = function(){
    return function(req, res, next){
        let a = [1];
        if(a.indexOf(req.user.userType) !== -1){
            next();
        }else{
            return res.status(constantObj.httpStatus.forbidden).jsonp({
                status: constantObj.httpStatus.forbidden,
                message: 'User not authorized'
            });
        }
    }
}

exports.isAdmin = function(){
    return function(req, res, next){
        let a = [2];
        if(a.indexOf(req.user.userType) !== -1){
            next();
        }else{
            return res.status(constantObj.httpStatus.forbidden).jsonp({
                status: constantObj.httpStatus.forbidden,
                message: 'User not authorized'
            });
        }
    }
}

exports.isAdminSubAdmin = function(){
    return function(req, res, next){
        let a = [2,3];
        if(a.indexOf(req.user.userType) !== -1){
            next();
        }else{
            return res.status(constantObj.httpStatus.forbidden).jsonp({
                status: constantObj.httpStatus.forbidden,
                message: 'User not authorized'
            });
        }
    }
}

exports.isCustomer = function(){
    return function(req, res, next){
        let a = [4];
        if(a.indexOf(req.user.userType) !== -1){
            next();
        }else{
            return res.status(constantObj.httpStatus.forbidden).jsonp({
                status: constantObj.httpStatus.forbidden,
                message: 'User not authorized'
            });
        }
    }
}

exports.checkAdminUser = function(){
    return function(req, res, next) {
        if(req && req.user){
            if(req.user.status){
                if(req.user.userType){
                    let a = [1,2,3];
                    if(a.indexOf(req.user.userType) !== -1){
                        next();
                    }else{
                        return res.status(constantObj.httpStatus.forbidden).jsonp({
                            status: constantObj.httpStatus.forbidden,
                            message: 'User not authorized'
                        });
                    }
                }else{
                    return res.status(constantObj.httpStatus.badRequest).jsonp({
                        status: constantObj.httpStatus.badRequest,
                        message: 'UserType not specified.'
                    });
                }
            }else{
                return res.status(constantObj.httpStatus.forbidden).jsonp({
                    status: constantObj.httpStatus.forbidden,
                    message: req.user.message
                });
            }
        }else{
            return res.status(constantObj.httpStatus.unauthorized).jsonp({
                status: constantObj.httpStatus.unauthorized,
                message: 'Oops, Some error occured! Please login again.'
            });
        }
    }
}

exports.checkNormalUser = function(){
    return function(req, res, next) {
        if(req && req.user){
            if(req.user.status){
                if(req.user.userType){
                    let a = [1,2,3];
                    if(req.user.userType === 3){
                        next();
                    }else{
                        return res.status(constantObj.httpStatus.forbidden).jsonp({
                            status: constantObj.httpStatus.forbidden,
                            message: 'Not authorized'
                        });
                    }
                }else{
                    return res.status(constantObj.httpStatus.badRequest).jsonp({
                        status: constantObj.httpStatus.badRequest,
                        message: 'UserType not specified.'
                    });
                }
            }else{
                return res.status(constantObj.httpStatus.forbidden).jsonp({
                    status: constantObj.httpStatus.forbidden,
                    message: req.user.message
                });
            }
        }else{
            return res.status(constantObj.httpStatus.unauthorized).jsonp({
                status: constantObj.httpStatus.unauthorized,
                message: 'Oops, Some error occured! Please login again.'
            });
        }
    }
}