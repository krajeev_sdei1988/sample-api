module.exports = function(app, express, passport) {
    const router = express.Router();
    const middlewareObj = require('./../app/policies/auth.js');
    const customerValObj = require('./../app/controllers/customers/customersVal.js');
    const customerObj = require('./../app/controllers/customers/customers.js');

    // router.post('/uploadCustomers', customerObj.convertCustomers);
    // router.post('/list', [customerValObj.listCustomersVal()], customerObj.listCustomers);
    router.post('/createFromAdmin', [customerValObj.createCustomerAdminVal()], customerObj.createCustomerAdmin);
    router.post('/create', [customerValObj.createCustomerAdminVal(true)], customerObj.createCustomer);
    // router.put('/edit', [customerValObj.editCustomerVal()], customerObj.editCustomer);
    // router.get('/getData/:_id', [customerValObj.getCustomerDataVal()], customerObj.getCustomerData);
    // router.post('/delete', [customerValObj.deleteCustomerSoftVal()], customerObj.deleteCustomerSoft);
    // router.post('/changeStatus', [customerValObj.changeCustomerStatusVal()], customerObj.changeCustomerStatus);
    
    app.use('/customer', router);
}