
module.exports = function(app, express, passport) {
  var router = express.Router();
  // var middlewareObj = require('./../app/policies/auth.js');
  // var loginObj = require('./../app/controllers/adminlogins/adminlogins.js');

  router.post('/login', passport.authenticate('userObj', {
      failureRedirect: '/login',
      failureFlash: true,
      session: true
  }));
  
  app.use('/auth', router);
}
