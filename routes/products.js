module.exports = function(app, express, passport) {
    const router = express.Router();
    const middlewareObj = require('./../app/policies/auth.js');
    const productValObj = require('./../app/controllers/products/productsVal.js');
    const productObj = require('./../app/controllers/products/products.js');

    router.post('/uploadProducts', productObj.convertProducts);
    router.post('/list', [productValObj.listProductsVal()], productObj.listProducts);
    router.post('/create', [productValObj.createProductVal()], productObj.createProduct);
    router.put('/edit', [productValObj.editProductVal()], productObj.editProduct);
    router.get('/getData/:_id', [productValObj.getProductDataVal()], productObj.getProductData);
    router.post('/delete', [productValObj.deleteProductSoftVal()], productObj.deleteProductSoft);
    router.post('/changeStatus', [productValObj.changeProductStatusVal()], productObj.changeProductStatus);
    
    app.use('/products', router);
}