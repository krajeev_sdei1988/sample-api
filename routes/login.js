module.exports = function(app, express, passport) {
    var router = express.Router();
    var middlewareObj = require('./../app/policies/auth.js');
    var loginObj = require('./../app/controllers/login/login.js');

    router.post('/admin', passport.authenticate('local', {session: true}), middlewareObj.checkAdminUser(),loginObj.authenticate);
    router.post('/customer', passport.authenticate('local', {session: true}), middlewareObj.checkNormalUser(), loginObj.authenticate);
    
    app.use('/auth', router);
}