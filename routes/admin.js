module.exports = function(app, express, passport) {
    var router = express.Router();
    var middlewareObj = require('./../app/policies/auth.js');
    // const superAdminDbObj = require('./../app/controllers/superAdmin/superAdmin.js');
    const adminObj = require('./../app/controllers/admin/admin.js');
    const adminValObj = require('./../app/controllers/admin/adminVal.js');

    router.post('/create', [middlewareObj.checkToken(), middlewareObj.isSupAdm(), adminValObj.createAdminVal()], adminObj.createAdmin);
    router.put('/edit', [middlewareObj.checkToken(), middlewareObj.isSupAdm(), adminValObj.editAdminVal()], adminObj.editAdmin)
    router.post('/list', [middlewareObj.checkToken(), middlewareObj.isSupAdm(), adminValObj.listAdminsVal()], adminObj.listAdmins);
    // router.post('/list', [adminValObj.listAdminsVal()], adminObj.listAdmins);
    router.get('/getData/:_id', [middlewareObj.checkToken(), middlewareObj.isSupAdm(), adminValObj.getAdminDataVal()], adminObj.getAdminData);
    router.post('/delete', [middlewareObj.checkToken(), middlewareObj.isSupAdm(), adminValObj.deleteAdminSoftVal()], adminObj.deleteAdminSoft);
    router.post('/changeStatus', [middlewareObj.checkToken(), middlewareObj.isSupAdm(), adminValObj.changeAdminStatusVal()], adminObj.changeAdminStatus);
    // router.post('/customer', passport.authenticate('local'), middlewareObj.checkNormalUser(), loginObj.authenticate);
    
    app.use('/admin', router);
}