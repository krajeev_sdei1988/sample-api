/*
 * The file will take care of the database connectivity
 */
var mongoose = require('mongoose');
//mongoose.connect('mongodb://UserName:Password@Host:27017/dbName');
// mongoose.connect('mongodb://localhost:27017/adminSampleDB');
 mongoose.connect('mongodb+srv://admin:admin@cluster0-bzdlq.mongodb.net/test?retryWrites=true&w=majority');

// check if we are connected successfully or not
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Error in connecting with database on :', new Date()));
db.on('connected', console.error.bind(console, 'Data base connected successfully on :', new Date()));
db.on('disconnected', console.error.bind(console, 'Data base disconnected successfully:', new Date()));