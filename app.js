global.Promise = require('bluebird');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var session = require('express-session');
var CryptoJS = require("crypto-js");
const constantObj = require('./constants.js')
var db = require('./db.js')
const LocalStrategy = require('passport-local').Strategy;
const cryptiles = require('cryptiles')

const tokenService = require('./app/services/authToken.js');
let loginObj = require('./app/models/login/loginModel.js')
const loginTokenObj = require('./app/models/login/loginToken.js');
let superAdminObj = require('./app/models/superAdmin/superAdminModel.js');
const adminObj = require('./app/models/admin/adminModel.js');
const companyObj = require('./app/models/company/companyModel.js');
const defaultDB = require('./app/services/populateDB.js');
const uploadDB = require('./app/services/uploadDB.js');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use(bodyParser.json({
	limit: '5000mb'
}));
app.use(bodyParser.urlencoded({
	limit: '5000mb',
	extended: true
}));

app.use(session({
	secret: '**picture-line**',
	resave: false,
	saveUninitialized: true,
}));
app.use(passport.initialize());
app.use(passport.session());



var swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc'); 
//let   options = require('./api-doc/info');
    swaggerDocument = require('./api-doc/info');
    
const swaggerSpec = swaggerJSDoc(swaggerDocument);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
// catch 404 and forward to error handler
// app.use(function (req, res, next) {
// 	next(createError(404));
// });

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

//CORS Setup
app.all('/*', function (request, response, next) {
	response.header("Access-Control-Allow-Origin", "*");
	response.header("Access-Control-Allow-Headers", "X-Requested-With");
	response.header("Access-Control-Allow-Methods", "GET, POST", "PUT", "DELETE");
	response.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
	response.header('Expires', '-1');
	response.header('Pragma', 'no-cache');
	next();
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

// ******************* Passport Integration ************************ //
function makeToken(req, adminData, done) {
    var myToken = tokenService.issueToken({
        username: adminData
    });
    var tokenObj = new loginTokenObj({
        "user": adminData._id,
        "token": myToken,
        "userType": adminData.userType,
    });
    tokenObj.save(function(e, s) {});
    adminData.myToken = myToken;
    return done(null, adminData);
}

let model;

passport.use('local', new LocalStrategy({
    passReqToCallback: true
}, function(req, username, password, done) {
    loginObj.findOne({
        email: username,
        isDeleted: false
    }).lean().exec(function(err, adminData) {
        if (err) {
			return done(err);
		}
        //if there is no such user corresponding to email
        if (!adminData) {
            return done(null, { success: false, message: "User doesn't exist" });
        }

        //if password not matched with database credentials
        if (!isValidPassword(adminData.password, password)) {
            return done(null, { success: false, message: 'Invalid Credentials.' }); 
        }

        //if user is blocked by admin
        if (adminData.status == false) {
            return done(null, { success: false, message: 'Blocked by admin.' });
        }else{
            makeToken(req, adminData, done)
        }
    });
}));

var isValidPassword = function(userPassword, password) {
    var bytes  = CryptoJS.AES.decrypt(userPassword.toString(), constantObj.cryptoSecret);
    var dbPassword = bytes.toString(CryptoJS.enc.Utf8);
    return cryptiles.fixedTimeComparison(dbPassword, password);
}

passport.serializeUser(loginObj.serializeUser);
passport.deserializeUser(loginObj.deserializeUser);

require('./routes/index')(app, express, passport);
require('./routes/login')(app, express, passport);
require('./routes/admin')(app, express, passport);
require('./routes/products')(app, express, passport);
require('./routes/customers')(app, express, passport);

// auto populate the Database with Super Admin, Company & Admin data.
defaultDB.populateDB();

module.exports = app;
