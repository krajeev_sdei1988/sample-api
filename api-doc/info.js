//import { routes, APP } from 'config';

const info = 
{
    apis: [
        'api-doc/*.api.yaml',
        //'api-doc/marchants.api.yaml',
        // 'admin-portal/schemas/*.schema.yaml',
        // 'schemas/utilitiy.schema.yaml',
        // 'common/merchant-profile.api.yaml',
        // 'common/schemas/*.schema.yaml'
    ],
    swaggerDefinition: {
        openapi: "3.0.0",
        info: {
            version: '1.0.0',
            title: 'Smaple Admin Portal APIs',
            termsOfService: 'http://swagger.io/terms/',
            description: 'This is a Sample project admin portal Api Documentation. You can find out more about Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).      For this sample, you can use the api key `special-key` to test the authorization filters.',
            contact: {
                name: "API Support",
                email: "krajeev@smartdatainc.net",
                url: "http://example.com/support"
            },
            license: {
                name: 'Apache 2.0',
                url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
            }
        },
        host:"localhost", //routes.HOST,
        basePath: "api/v1",//routes.BASE_PATH,
        tags: [
            {
                name: 'admin',
                description: 'Admin Functionality',
                externalDocs: {
                    description: 'M-s: Admin, Sprint: 2'
                }
            },
            {
                name: 'merchants',
                description: 'Merchants Functionality',
                externalDocs: {
                    description: 'M-s: Merchants, Sprint: 2'
                }
            },
            {
                name: 'regularDeals',
                description: 'Regular Deals Functionality',
                externalDocs: {
                    description: 'M-s: Merchants, Sprint: 2'
                }
            },
            {
                name: 'activities',
                description: 'Activities Functionality',
                externalDocs: {
                    description: 'M-s: Activities, Sprint: 2'
                }
            },
            {
                name: 'freeActivitySuggestion',
                description: 'Free Activity Suggestion Functionality',
                externalDocs: {
                    description: 'M-s: Activities, Sprint: 2'
                }
            },
            {
                name: 'freeActivitySuggestionCategory',
                description: 'Free Activity Suggestion Category Functionality',
                externalDocs: {
                    description: 'M-s: Activities, Sprint: 2'
                }
            },
            {
                name: 'merchantActivity',
                description: 'Merchant Activity Functionality',
                externalDocs: {
                    description: 'M-s: Activities, Sprint: 2'
                }
            },
            {
                name: 'activityOutlet',
                description: 'Activity Outlet Functionality',
                externalDocs: {
                    description: 'M-s: Activities, Sprint: 2'
                }
            }
        ],
        schemes: ['http'],
        externalDocs: {
            description: 'Find out more about Swagger',
            url: 'http://swagger.io'
        }
    }
};

module.exports = info;
